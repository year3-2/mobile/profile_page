import 'package:flutter/material.dart';
enum APP_THEME { LIGHT, DARK }
void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
        brightness: Brightness.light,
        appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(color: Colors.orange.shade800),
        ),
        iconTheme: IconThemeData(color: Colors.blueAccent));
  }
  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        appBarTheme: AppBarTheme(
          color: Colors.black12,
          iconTheme: IconThemeData(color: Colors.yellow),
        ),
        iconTheme: IconThemeData(color: Colors.orange.shade400));
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}


class _ContactProfilePageState extends State<ContactProfilePage> {
  var currnetTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currnetTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home:Scaffold(
        appBar:buildAppBarWidget(),
        body:buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.color_lens_outlined),
          onPressed: (){
            setState(() {
              currnetTheme == APP_THEME.DARK
                  ? currnetTheme = APP_THEME.LIGHT
                  : currnetTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }
}

AppBar buildAppBarWidget(){
  return AppBar(
    //backgroundColor: Colors.pinkAccent,
    leading: Icon(Icons.arrow_back_ios),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.orange,
      )
    ],
  );
}

Widget buildBodyWidget(){
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: 250,
            child: Image.network("https://scontent.fbkk23-1.fna.fbcdn.net/v/t1.6435-9/127273732_2067692526698494_8432957825247119408_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeFPMa8IMfansFVkg1CNfxWn1S_-zhb4ofzVL_7OFvih_F-6RaBswIzpWjX-U1Pvn4dFHsRD4ssHgYASncdZhYt3&_nc_ohc=CS7unuxEMzAAX-F7D8r&_nc_ht=scontent.fbkk23-1.fna&oh=00_AfAyJotqAzG7FkvWzQiNscs7QX9DkK6A8PX9CyI8NVVe3A&oe=63CCA174",
                fit:BoxFit.cover),
          ),
          Container(
            height: 68,
            child:Row(
              children: <Widget>[
                Text("  NTC JS",style: TextStyle(fontSize: 30),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.grey,
          ),

          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
              child: Theme(
                data:ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.cyan,
                ),
              ),
              child:profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.grey,
          ),
          mobilephoneListTile(),
          otherPhoneListTile(),
          emailListTile(),
          addressListTile(),
        ],
      )
    ],
  );

}
Widget profileActionItems(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideocallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}
Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
           //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}
Widget buildVideocallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.mail,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}
Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //color: Colors.pink,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget mobilephoneListTile(){
  return ListTile(
    leading:Icon(
    Icons.call),
    title: Text("000-111-2222"),
    subtitle:Text("mobile"),
    trailing: IconButton(icon:Icon(Icons.message),
  color: Colors.pink,onPressed: (){},
  ),
  );
}
Widget otherPhoneListTile(){
  return ListTile(
    leading:Text(""),
    title: Text("440-440-3290"),
    subtitle:Text("other"),
    trailing: IconButton(icon:Icon(Icons.message),
      color: Colors.pink,onPressed: (){},
    ),
  );
}
Widget emailListTile(){
  return ListTile(
    leading:Icon(
        Icons.email),
    title: Text("63160004@go.buu.ac.th"),
    subtitle:Text("work"),
  );
}
Widget addressListTile(){
  return ListTile(
    leading:Icon(
        Icons.location_pin),
    title: Text("57/11 CHonburi"),
    subtitle:Text(""),
    trailing: IconButton(icon:Icon(Icons.directions),
      color: Colors.pink,onPressed: (){},
    ),
  );
}









